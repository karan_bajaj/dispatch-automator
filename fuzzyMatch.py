from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import csv
import pdb

CHOP_CHARS = 32

fefilestr = 'durgesh&jasmeet'

csvfile = open('addressmaster.csv', 'rb')
outfile = open(fefilestr+'_mapped.csv', 'w')

csvlines = csv.reader(csvfile, delimiter=',', quotechar='|')

fe_list = []
fe_file = open(fefilestr+'.txt', 'rb')
for line in fe_file:
	fe_list.append(line)

for row in csvlines:
	try:
		client_code = row[0]
		client_name = row[1]
		addr1 = row[2]
		addr2 = row[3]
		
		client_name_matched = ""
		name_matched = False
		client_address_matched = False
		# for a particular client - address
		for idx in range(0, len(fe_list)):
			# when client name matches
			if(fuzz.ratio(client_name, fe_list[idx]) > 75):
				client_name_matched = fe_list[idx].rstrip()
				name_matched = True
				# find next two non-empty lines
				a1 = "";
				a2 = "";
				for i in range(idx+1, len(fe_list)):
					if(fe_list[i] != ""):
						if(a1 == ""):
							a1 = fe_list[i].rstrip().replace(",", " ")
						elif(a2 == ""):
							a2 = fe_list[i].rstrip().replace(",", " ")
							break
				# when address matches
				if(fuzz.ratio(addr1, a1) > 50 or
					fuzz.ratio(addr1+addr2, a1+a2) > 50 or
					fuzz.ratio(addr1+addr2, a1) > 50):
					print(client_name)
					outfile.write(client_code + "," + client_name + "," + (addr1 + " " + addr2)[:CHOP_CHARS] + ",FOUND\n," \
						+ client_name_matched + "," + (a1 + " " + a2)[:CHOP_CHARS] + "\n")
					client_address_matched = True
					break
		if(client_address_matched == False and name_matched == True):
			outfile.write(client_code + "," + client_name + "," + (addr1 + " " + addr2)[:CHOP_CHARS] + "\n," \
				+ client_name_matched + "\n")

	except Exception as e:
		print(e)
	else:
		pass
	finally:
		pass